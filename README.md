# WhatTheTime
Adds a full date and time item to the MacOS menu

![Screenshot Image](images/screenshot.png "Screenshot Image")

## Download
Go to the [releases page](https://gitlab.com/GeneralistDev/WhatTheTime/-/releases) and download the latest .pkg file
