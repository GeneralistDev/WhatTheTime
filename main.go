package main

import (
	"runtime"
	"time"

	"github.com/progrium/macdriver/cocoa"
	"github.com/progrium/macdriver/objc"
)

func currentTime() string {
	t := time.Now()
	return t.Format("Monday, 02 January 2006 15:04:05 -0700")
}

func main() {
	runtime.LockOSThread()

	app := cocoa.NSApp_WithDidLaunch(func(n objc.Object) {
		obj := cocoa.NSStatusBar_System().StatusItemWithLength(cocoa.NSVariableStatusItemLength)
		obj.Retain()
		go func() {
			dateText := currentTime()
			for {
				select {
					case <-time.After(100 * time.Millisecond):
						dateText = currentTime()
				}

				obj.Button().SetTitle(dateText)
			}
		}()


		itemQuit := cocoa.NSMenuItem_New()
		itemQuit.SetTitle("Quit")
		itemQuit.SetAction(objc.Sel("terminate:"))

		menu := cocoa.NSMenu_New()
		menu.AddItem(itemQuit)

		obj.SetMenu(menu)
	})
	app.Run()
}